x2go.backends package
=====================

Subpackages
-----------

.. toctree::

    x2go.backends.control
    x2go.backends.info
    x2go.backends.printing
    x2go.backends.profiles
    x2go.backends.proxy
    x2go.backends.settings
    x2go.backends.terminal

Module contents
---------------

.. automodule:: x2go.backends
    :members:
    :undoc-members:
    :show-inheritance:
