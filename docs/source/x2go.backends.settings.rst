x2go.backends.settings package
==============================

Submodules
----------

.. toctree::

   x2go.backends.settings.file

Module contents
---------------

.. automodule:: x2go.backends.settings
    :members:
    :undoc-members:
    :show-inheritance:
