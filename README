python-x2go - Copyright (C) 2010-2023 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>

Published under the terms of the GNU Affero General Public License.
See http://www.gnu.org/licenses/agpl.html for a recent copy.


=== What is Python X2Go? ===

The Python X2Go module integrates X2Go client support into your python programmes.
Python X2Go is used in the headless X2Go client »pyhoca-cli« and by the tiny systray GUI
applet »pyhoca-gui«

Python X2Go takes advantages of the NX Proxy by NoMachine published under GPL.
On Windows and MacOS X systems you have to make sure that you have installed a current
NX Proxy executable somewhere on your system. It probably will be sufficient to have
the X2Go project's X2Go Client application installed on your client machine.

On Debian / Ubuntu systems there is an nxproxy package available within the distribution.
Python X2Go defaults to using the distro nxproxy binary.

Python X2Go was originally inspired by work of Jörg Sawatzki <joerg.sawatzki@web.de>.
Unfortunately, Jörg had to give up work on his ideas due to licensing issues with his
contractor. To make his ideas furthermore available to the OSS community this complete
rewrite of Jörg's ideas has been called into life.

In case you have the opinion that parts of the presented code are not as much a rewrite
as they should be, may I ask you to contact me directly and in person. I am sure, things can
be sorted out in a non-complicated and heartful fashion. Thanks in advance for your directness
and wholeheartedness concerning this inner process.


=== Requirements ===

* X2Go Server
    - you need a working X2Go server to connect to. In order to use shadowing or mTelePlayer
      make sure you have at least X2Go Server (>= 4.1.0.0) installed
* on the client system you will need
    - the python-gevent library (for details refer to: http://www.gevent.org/)
    - the python-paramiko library (SSH implementation in Python, http://www.lag.net/paramiko/)
    - NX Proxy for non-Debianized systems
* further recommendations about printing on Windows systems
    - optimal: install Ghostscript and GSView in the default paths. If these tools are installed
      they will be used for printing. Further infos and downloads: http://www.ghostscript.com/
    - also working: if no Ghostscript/GSView is installed the win32api ,,print'' command will
      be executed on incoming PDF print spool files. Win32api will try launch the print function
      of the default PDF viewer application (mostly Adobe Acrobat Reader)
    - if you have a choice: install Ghostscript/GSview on the system that uses Python X2Go
      applications... it's highly recommended


=== Current features ===

* start X2Go agent session
* suspend X2Go session
* resume X2Go session
* terminate X2Go session
* clean user's X2Go sessions
* list user's X2Go sessions
* use Pulse Audio sound
* X2Go printing
* reading/writing session profiles from file
* sharing of local (client-side) folders (SFTP server is integrated in Python X2Go)
* connect via proxy SSH server
* X2Go MIME box support
* color depth auto-recognition
* X2Go desktop sharing support
* X2Go published applications support
* Session window re-titling for desktop and shared desktop sessions
* X2Go Session Brokerage (HTTP or HTTPS)
* Telekinesis Client support included (required for multimedia support inside
  the X2Go session using mTelePlayer as media player)

=== Installation ===

Ubuntu:
-------
We use Launchpad for Ubuntu packaging:

  $ add-apt-repository ppa:x2go/stable
  $ apt-get update
  $ apt-get install python-x2go

From Ubuntu oneiric on python-x2go is also available as a part of the
Ubuntu GNU/Linux distribution.


Debian:
-------
create /etc/apt/sources.list.d/x2go.list and put the following line into it:

<snip>
deb http://packages.x2go.org/debian {squeeze|wheezy|sid} main
</snip>

Then run the following commands as super-user root:

  $ apt-get update
  $ apt-get install python-x2go


Windos:
-------
Testing and writing a short howto for this section is very welcome.

MacOS X:
--------
Testing and writing a short howto for this section is very welcome.


=== How to use it from the command line? ===

A good example for the usage of Python X2Go is the pyhoca-cli programme, a headless
X2Go client that aims at 100% compatibility with X2Go Client applications released in
the X2Go project.

On Debian/Ubuntu you can easily install the X2Go Client by running APT again:

  $ apt-get install pyhoca-cli


=== How to use it with a GUI? ===

There is also a Python GUI application called PyHoca-GUI. PyHoca-GUI is an applet that docks into your
desktop's system tray.

  $ apt-get install pyhoca-gui


=== Repository and Feedback ===

Before giving feedback take a look at http://git.x2go.org/ (project: python-x2go.git) and
check if your issues have already been solved in the HEAD of python-x2go code.

For now, bugs can be reported via mail to mike.gabriel@das-netzwerkteam.de


light+love, 20142010
Mike Gabriel <mike.gabriel@das-netzwerkteam.de>

